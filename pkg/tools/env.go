package tools

import "os"

func GetEnv(key, defaultVal string) string {
	if v, exist := os.LookupEnv(key); exist {
		return v
	}

	return defaultVal
}
