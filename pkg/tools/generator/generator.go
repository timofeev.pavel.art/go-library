package generator

import (
	"log"
	"main/pkg/models"
	"math/rand"

	"github.com/jmoiron/sqlx"

	"github.com/brianvoe/gofakeit/v6"
)

func GenerateData(db *sqlx.DB) (err error) {
	var authors []models.Author
	if err = GenerateUsers(db); err != nil {
		return err
	}

	err, authors = GenerateAuthors(db)
	if err != nil {
		return err
	}

	if err = GenerateBooks(authors, db); err != nil {
		return err
	}

	log.Println("Database initialization complete.")
	return nil
}

func GenerateUsers(db *sqlx.DB) error {
	var (
		count int
		err   error
	)
	err = db.QueryRow("SELECT count(*) FROM users").Scan(&count)
	if err != nil {
		return err
	}

	if count != 0 {
		return nil
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt := "INSERT INTO users (USERNAME, FIRSTNAME, LASTNAME) VALUES ($1, $2, $3)"
	amount := rand.Intn(50) + 50

	for i := 0; i != amount; i++ {
		username := gofakeit.Username()
		firstName := gofakeit.FirstName()
		lastName := gofakeit.LastName()

		_, err = tx.Exec(stmt, username, firstName, lastName)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}

	log.Println("Users generated")
	return tx.Commit()
}

func GenerateAuthors(db *sqlx.DB) (error, []models.Author) {
	var (
		count int
		err   error
	)
	err = db.QueryRow("SELECT count(*) FROM authors").Scan(&count)
	if err != nil {
		return err, nil
	}

	if count != 0 {
		return nil, nil
	}

	tx, err := db.Begin()
	if err != nil {
		return err, nil
	}

	stmt := "INSERT INTO authors (AUTHOR_NAME) VALUES ($1)"
	authors := make([]models.Author, 10)

	for i := range authors {
		name := gofakeit.Name()
		authors[i].ID = i + 1
		authors[i].Name = name

		_, err = tx.Exec(stmt, name)
		if err != nil {
			_ = tx.Rollback()
			return err, nil
		}
	}

	log.Println("Authors generated")
	return tx.Commit(), authors
}

func GenerateBooks(authors []models.Author, db *sqlx.DB) error {
	var (
		count int
		err   error
	)
	err = db.QueryRow("SELECT count(*) FROM books").Scan(&count)
	if err != nil {
		return err
	}

	if count != 0 {
		return nil
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt := "INSERT INTO books (BOOK_NAME, AUTHOR_ID) VALUES ($1, $2)"

	for i := 0; i != 100; i++ {
		name := gofakeit.Sentence(rand.Intn(4) + 1)
		author := authors[rand.Intn(len(authors)-1)]

		_, err = tx.Exec(stmt, name, author.ID)
		if err != nil {
			_ = tx.Rollback()
			return err
		}
	}

	log.Println("Books generated")
	return tx.Commit()
}
