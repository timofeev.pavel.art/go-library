package models

type Book struct {
	ID     int    `json:"id" db:"book_id"`
	Name   string `json:"name" db:"book_name"`
	Author `json:"author"`
}

type BookSimlify struct {
	ID   int    `json:"id" db:"book_id"`
	Name string `json:"name" db:"book_name"`
}
