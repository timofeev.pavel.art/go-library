package models

type RentedBook struct {
	ID   int  `json:"id" db:"rentbook_id"`
	User User `json:"user"`
	Book Book `json:"book"`
}
