package models

type User struct {
	ID          int    `json:"id" db:"user_id"`
	Username    string `json:"username"`
	FirstName   string `json:"firstname"`
	LastName    string `json:"lastname"`
	RentedBooks []Book `json:"rentedBooks"`
}
