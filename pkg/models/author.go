package models

type Author struct {
	ID   int    `json:"id" db:"author_id"`
	Name string `json:"name" db:"author_name"`
}

type AuthorsBooks struct {
	Author
	Books []BookSimlify `json:"books"`
}
