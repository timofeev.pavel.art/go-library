package handler

import (
	"encoding/json"
	"main/pkg/models"
	"net/http"
)

// swagger:route POST /author author createAuthorRequest
// Create author
// responses:
//	200: createAuthorResponse
//	400: description: Bad Request

func (h *Handler) CreateAuthor(w http.ResponseWriter, r *http.Request) {
	var (
		author models.Author
		err    error
		name   string
	)

	name = r.FormValue("name")
	author.Name = name

	if err = h.services.Author.Create(author); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

// swagger:route GET /author author getAllAuthorsRequest
// Get all authors
// responses:
//
//	200: getAllAuthorsResponse
//	400: description: Bad Request
//	500: description: Internal Server Error
func (h *Handler) GetAllAuthors(w http.ResponseWriter, r *http.Request) {
	list, err := h.services.Author.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err = json.NewEncoder(w).Encode(list); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
