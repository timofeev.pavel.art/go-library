package handler

import (
	"encoding/json"
	"main/pkg/models"
	"net/http"
	"strconv"
)

// swagger:route POST /book book createBookRequest
// Create book
// responses:
//	200: createBookResponse
//	400: description: Bad Request

func (h *Handler) CreateBook(w http.ResponseWriter, r *http.Request) {
	var (
		book  models.Book
		err   error
		name  string
		idRaw string
		id    int
	)

	name = r.FormValue("name")
	idRaw = r.FormValue("id")
	id, err = strconv.Atoi(idRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	book.Name = name
	book.Author.ID = id

	if err = h.services.Book.Create(book); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

// swagger:route GET /book book getAllBooksRequest
// Get all books
// responses:
//
//	200: getAllBooksResponse
//	400: description: Bad Request
//	500: description: Internal Server Error
func (h *Handler) GetAllBooks(w http.ResponseWriter, r *http.Request) {
	list, err := h.services.Book.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err = json.NewEncoder(w).Encode(list); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
