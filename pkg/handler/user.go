package handler

import (
	"encoding/json"
	"net/http"
)

// swagger:route GET /users users getAllUsersRequest
// Get all users with rented books field
// responses:
//	200: getAllUsersResponse
//	500: description: Internal Server Error

func (h *Handler) GetAllUsers(w http.ResponseWriter, r *http.Request) {
	list, err := h.services.User.GetAll()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if err = json.NewEncoder(w).Encode(list); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
