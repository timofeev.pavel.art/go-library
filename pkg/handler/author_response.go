// nolint:all
package handler

import "main/pkg/models"

// swagger:response
type createAuthorResponse struct {
}

// swagger:response
type getAllAuthorsResponse struct {
	// In:body
	Body []models.AuthorsBooks
}
