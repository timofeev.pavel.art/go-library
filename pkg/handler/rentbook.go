package handler

import (
	"net/http"
	"strconv"
)

//swagger:route POST /rent rent createRentRequest
// Add to user book
// responses:
//	200: createRentResponse
//	400: description: Bad Request
//	404: description: Not Found

func (h *Handler) CreateRentBook(w http.ResponseWriter, r *http.Request) {
	var (
		err                  error
		bookIdRaw, userIdRaw string
		bookId, userId       int
	)

	bookIdRaw = r.FormValue("bookId")
	userIdRaw = r.FormValue("userId")

	bookId, err = strconv.Atoi(bookIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	userId, err = strconv.Atoi(userIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = h.services.RentBook.Create(bookId, userId); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}

//swagger:route DELETE /rent rent deleteRentRequest
// Return book from user
// responses:
//	200: deleteRentResponse
//	400: description: Bad Request
//	404: description: Not Found

func (h *Handler) DeleteRentBook(w http.ResponseWriter, r *http.Request) {
	var (
		err                  error
		bookIdRaw, userIdRaw string
		bookId, userId       int
	)

	bookIdRaw = r.URL.Query()["bookId"][0]
	userIdRaw = r.URL.Query()["userId"][0]

	bookId, err = strconv.Atoi(bookIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	userId, err = strconv.Atoi(userIdRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = h.services.RentBook.Delete(bookId, userId); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
}
