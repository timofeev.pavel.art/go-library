// nolint:all
package handler

// swagger:response createBookResponse
type createBookResponse struct {
}

// swagger:response getAllBooksResponse
type getAllBooksResponse struct {
	// In: body
	ID int `json:"id"`
}
