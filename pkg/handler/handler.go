package handler

import (
	"main/pkg/service"
	"net/http"

	"github.com/go-chi/chi/middleware"

	"github.com/go-chi/chi"
)

type Handler struct {
	services *service.Service
}

func NewHandler(service *service.Service) *Handler {
	return &Handler{services: service}
}

func (h *Handler) Routes() chi.Router {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	r.Get("/swagger", swaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./doc"))).ServeHTTP(w, r)
	})

	r.Post("/book", h.CreateBook)
	r.Get("/book", h.GetAllBooks)

	r.Post("/author", h.CreateAuthor)
	r.Get("/author", h.GetAllAuthors)

	r.Get("/users", h.GetAllUsers)

	r.Post("/rent", h.CreateRentBook)
	r.Delete("/rent", h.DeleteRentBook)

	return r
}
