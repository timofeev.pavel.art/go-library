// nolint:all
package handler

import "main/pkg/repository"

// swagger:response getAllUsersResponse
type getAllUsersResponse struct {
	// In:body
	Body []repository.User
}
