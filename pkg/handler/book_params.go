// nolint:all
package handler

// swagger:parameters createBookRequest
type createBookRequest struct {
	// Book title
	// In: formData
	Name string `json:"name"`
	// Author ID
	// In: formData
	ID int `json:"id"`
}

// swagger:parameters getAllBooksRequest
type getAllBooksRequest struct {
}
