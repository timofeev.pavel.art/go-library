// nolint:all
package handler

// swagger:parameters createRentRequest
type createRentRequest struct {
	// User ID
	// In:formData
	UserID int `json:"userId"`

	// Book ID
	// In:formData
	BookID int `json:"bookId"`
}

// swagger:parameters deleteRentRequest
type deleteRentRequest struct {
	// User ID
	// In:query
	UserID int `json:"userId"`

	// Book ID
	// In:query
	BookID int `json:"bookId"`
}
