// nolint:all
package handler

// swagger:parameters createAuthorRequest
type createAuthorRequest struct {
	// Author name
	// In: formData
	Name string `json:"name"`
}

// swagger:parameters getAllAuthorsRequest
type getAllAuthorsRequest struct {
}
