package service

import (
	"main/pkg/models"
	"main/pkg/repository"
)

type UserService struct {
	Repository repository.User
}

func NewUserService(repo repository.User) UserService {
	return UserService{Repository: repo}
}

func (u UserService) GetAll() ([]models.User, error) {
	return u.Repository.GetAll()
}
