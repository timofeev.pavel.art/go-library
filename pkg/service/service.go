package service

import (
	"main/pkg/models"
	"main/pkg/repository"
)

type Author interface {
	Create(auth models.Author) error
	GetAll() ([]models.AuthorsBooks, error)
}

type Book interface {
	Create(book models.Book) error
	GetAll() ([]models.Book, error)
}

type RentBook interface {
	Create(BookID, UserID int) error
	Delete(BookID, UserID int) error
}

type User interface {
	GetAll() ([]models.User, error)
}

type Service struct {
	Author
	Book
	RentBook
	User
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Author:   NewAuthorService(repos.Author),
		Book:     NewBookService(repos.Book),
		RentBook: NewRentBookService(repos.RentBook),
		User:     NewUserService(repos.User),
	}
}
