package service

import (
	"main/pkg/models"
	"main/pkg/repository"
)

type AuthorService struct {
	Repository repository.Author
}

func NewAuthorService(repo repository.Author) *AuthorService {
	return &AuthorService{Repository: repo}
}

func (a AuthorService) Create(auth models.Author) error {
	return a.Repository.Create(auth)
}

func (a AuthorService) GetAll() ([]models.AuthorsBooks, error) {
	return a.Repository.GetAll()
}
