package service

import (
	"main/pkg/repository"
)

type RentBookService struct {
	Repository repository.RentBook
}

func NewRentBookService(repo repository.RentBook) RentBookService {
	return RentBookService{Repository: repo}
}

func (r RentBookService) Create(BookID, UserID int) error {
	return r.Repository.Create(BookID, UserID)
}

func (r RentBookService) Delete(BookID, UserID int) error {
	return r.Repository.Delete(BookID, UserID)
}
