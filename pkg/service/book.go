package service

import (
	"main/pkg/models"
	"main/pkg/repository"
)

type BookService struct {
	Repository repository.Book
}

func NewBookService(repo repository.Book) *BookService {
	return &BookService{Repository: repo}
}

func (b BookService) Create(book models.Book) error {
	return b.Repository.Create(book)
}

func (b BookService) GetAll() ([]models.Book, error) {
	return b.Repository.GetAll()
}
