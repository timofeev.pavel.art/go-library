package repository

import (
	"fmt"
	"main/pkg/models"

	"github.com/jmoiron/sqlx"
)

type BookRepository struct {
	db *sqlx.DB
}

func NewBookRepository(db *sqlx.DB) *BookRepository {
	return &BookRepository{db: db}
}

func (b *BookRepository) Create(book models.Book) error {
	tx, err := b.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO books (BOOK_NAME, AUTHOR_ID) VALUES ($1, $2)")
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	if _, err = stmt.Exec(book.Name, book.Author.ID); err != nil {
		_ = tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (b *BookRepository) GetAll() (books []models.Book, err error) {
	if err = b.db.Select(&books, `
			SELECT *
			FROM books AS b
			INNER JOIN authors a ON a.author_id = b.author_id`); err != nil {
		return nil, err
	}

	if books == nil {
		return nil, fmt.Errorf("book list is empty")
	}

	return books, nil
}
