package repository

import (
	"fmt"
	"log"
	"main/pkg/models"

	"github.com/jmoiron/sqlx"
)

type UserRepository struct {
	db *sqlx.DB
}

func NewUserRepository(db *sqlx.DB) *UserRepository {
	return &UserRepository{db: db}
}

func (u *UserRepository) GetAll() (users []models.User, err error) {
	if err = u.db.Select(&users, "SELECT * FROM users"); err != nil {
		return nil, err
	}

	for i := range users {
		if err = u.db.Select(&users[i].RentedBooks, `
			SELECT b.book_id, b.book_name, a.author_id, a.author_name
			FROM rentbook
			INNER JOIN books b ON b.book_id = rentbook.book_id
			INNER JOIN authors a on a.author_id = b.author_id
			WHERE user_id = $1`, users[i].ID); err != nil {
			log.Println(err)
		}
	}

	if users == nil {
		return nil, fmt.Errorf("users list is empty")
	}

	return users, nil
}
