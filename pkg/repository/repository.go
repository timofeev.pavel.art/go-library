package repository

import (
	"main/pkg/models"

	"github.com/jmoiron/sqlx"
)

type Author interface {
	Create(auth models.Author) error
	GetAll() ([]models.AuthorsBooks, error)
}

type Book interface {
	Create(book models.Book) error
	GetAll() ([]models.Book, error)
}

type RentBook interface {
	Create(BookID, UserID int) error
	Delete(BookID, UserID int) error
}

type User interface {
	GetAll() ([]models.User, error)
}

type Repository struct {
	Author
	Book
	RentBook
	User
}

func NewRepository(db *sqlx.DB) *Repository {
	return &Repository{
		Author:   NewAuthorRepository(db),
		Book:     NewBookRepository(db),
		RentBook: NewRentBookRepository(db),
		User:     NewUserRepository(db),
	}
}
