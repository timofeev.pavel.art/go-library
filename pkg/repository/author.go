package repository

import (
	"fmt"
	"main/pkg/models"

	"github.com/jmoiron/sqlx"
)

type AuthorRepository struct {
	db *sqlx.DB
}

func NewAuthorRepository(db *sqlx.DB) *AuthorRepository {
	return &AuthorRepository{db: db}
}

func (a AuthorRepository) Create(auth models.Author) error {
	tx, err := a.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare(`
		INSERT INTO authors (AUTHOR_NAME)
		VALUES ($1)`)
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	if _, err = stmt.Exec(auth.Name); err != nil {
		_ = tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (a AuthorRepository) GetAll() (authors []models.AuthorsBooks, err error) {
	if err = a.db.Select(&authors, "SELECT * FROM authors"); err != nil {
		return nil, err
	}

	for i, item := range authors {
		var books []models.BookSimlify
		if err = a.db.Select(&books, `SELECT book_id, book_name FROM books WHERE AUTHOR_ID = $1`, item.ID); err != nil {
			return nil, err
		}
		authors[i].Books = books
	}

	if authors == nil {
		return nil, fmt.Errorf("author list is empty")
	}

	return authors, nil
}
