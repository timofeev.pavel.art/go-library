package repository

import (
	"fmt"
	"main/pkg/tools"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Config struct {
	host string
	port string
	usr  string
	pwd  string
	db   string
}

func GetCfg() Config {
	var cfg Config
	cfg.host = tools.GetEnv("DB_HOSTNAME", "")
	cfg.port = tools.GetEnv("DB_PORT_INT", "")
	cfg.usr = tools.GetEnv("DB_USR", "")
	cfg.pwd = tools.GetEnv("DB_PWD", "")
	cfg.db = tools.GetEnv("DB_DB", "")

	return cfg
}

func NewDatabase() (*sqlx.DB, error) {
	cfg := GetCfg()
	db, err := sqlx.Open("postgres", fmt.Sprintf(`host=%s port=%s user=%s password=%s dbname=%s sslmode=disable`,
		cfg.host, cfg.port, cfg.usr, cfg.pwd, cfg.db))

	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		return nil, err
	}

	return db, nil
}
