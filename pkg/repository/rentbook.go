package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

type RentBookRepository struct {
	db *sqlx.DB
}

func NewRentBookRepository(db *sqlx.DB) *RentBookRepository {
	return &RentBookRepository{db: db}
}

func (r *RentBookRepository) Create(BookID, UserID int) error {
	tx, err := r.db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO rentbook (USER_ID, BOOK_ID) VALUES ($1, $2)")
	if err != nil {
		_ = tx.Rollback()
		return err
	}

	if _, err = stmt.Exec(UserID, BookID); err != nil {
		_ = tx.Rollback()
		return err
	}

	return tx.Commit()
}

func (r *RentBookRepository) Delete(BookID, UserID int) error {
	result, err := r.db.Exec(`
					DELETE FROM rentbook
					 WHERE USER_ID = $1 AND BOOK_ID = $2`, UserID, BookID)
	if err != nil {
		return err
	}

	count, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if count == 0 {
		return fmt.Errorf("wrong user_id or book_id")
	}

	return nil
}
