CREATE TABLE  IF NOT EXISTS users
(
    USER_ID SERIAL PRIMARY KEY,
    USERNAME    CHAR(255),
    FIRSTNAME   CHAR(150),
    LASTNAME    CHAR(150)
);

CREATE TABLE IF NOT EXISTS authors
(
    AUTHOR_ID      SERIAL PRIMARY KEY,
    AUTHOR_NAME    CHAR(255)
);

CREATE TABLE IF NOT EXISTS books
(
    BOOK_ID         SERIAL PRIMARY KEY,
    BOOK_NAME        CHAR(255),
    AUTHOR_ID   INT,
    FOREIGN KEY (AUTHOR_ID) REFERENCES authors (AUTHOR_ID) ON DELETE SET NULL
);

CREATE TABLE IF NOT EXISTS rentbook
(
    RENTBOOK_ID      SERIAL PRIMARY KEY,
    USER_ID INT NOT NULL,
    BOOK_ID INT NOT NULL UNIQUE ,
    FOREIGN KEY (USER_ID) REFERENCES users (USER_ID),
    FOREIGN KEY (BOOK_ID) REFERENCES books (BOOK_ID)
);