ifneq (,$(wildcard ./.env))
	include .env
	export
endif
build:
	docker-compose build go-library
run:
	docker-compose up go-library
migrate-up:
	migrate -path ./schema -database 'postgres://${DB_USR}:${DB_PWD}@${DB_HOSTNAME}:${DB_PORT_INT}/${DB_DB}?sslmode=disable' -verbose up

migrate-down:
	migrate -path ./schema -database 'postgres://${DB_USR}:${DB_PWD}@${DB_HOSTNAME}:${DB_PORT_INT}/${DB_DB}?sslmode=disable' -verbose down