package main

import (
	"context"
	"fmt"
	"log"
	"main/pkg/tools/generator"
	"os"
	"os/signal"
	"time"

	"main/pkg/handler"
	"main/pkg/repository"
	"main/pkg/service"
	"main/pkg/tools"

	"github.com/jmoiron/sqlx"
	"github.com/joho/godotenv"
)

func init() {
	if err := godotenv.Load(); err != nil {
		log.Fatal(".env not found")
	}
}

func getController(db *sqlx.DB) *handler.Handler {
	repo := repository.NewRepository(db)
	serv := service.NewService(repo)
	handle := handler.NewHandler(serv)

	return handle
}

func main() {
	var (
		db     *sqlx.DB
		handle *handler.Handler
		server tools.Server
		err    error
		port   = fmt.Sprintf(":%v", tools.GetEnv("PORT", ""))
	)

	db, err = repository.NewDatabase()
	if err != nil {
		log.Fatal(err)
	}

	handle = getController(db)
	log.Println("Start database initialization")
	if err = generator.GenerateData(db); err != nil {
		log.Fatal(err)
	}

	go func() {
		if err = server.Run(handle.Routes(), port); err != nil {
			log.Fatal(err)
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err = server.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting.")
}
